import * as Discord from 'discord.js';
import * as _ from 'lodash';
import { redis } from './redis';
import RoleReactionWorker from './reaction';

// pass a discord.js message to it and get an array of emojis back (or null if no emojis)
export interface RedisReactionField {
  emoji: string
  role: string
}
export interface RedisReactionObject {
  channelId: string
  messageId: string
  guildId: string
  reactions?: RedisReactionField[]
}

const { TOKEN } = process.env;
if (!TOKEN) {
  console.error("No TOKEN present in environment");
  process.exit(-1);
}

class Bot {
  private token: string
  private client: Discord.Client
  private reactionWorker: Map<string, RoleReactionWorker>

  constructor(token: string) {
    this.token = token;
    this.reactionWorker = new Map<string, RoleReactionWorker>();
  }

  async Login() {
    this.client = new Discord.Client();
    
    this.client.on('ready', this.onReady);
    this.client.on('message', this.onMessage.bind(this));
    this.client.on('messageDelete', (msg) => {
      redis.del(msg.id)
      const worker = this.reactionWorker.get(msg.id);
      if (worker) {
        worker.Destroy();
        this.reactionWorker.delete(msg.id);
      }
    });
    

    /* this.client.on('messageReactionAdd', this.onMessageReactionAdd);
    this.client.on('messageReactionRemove', this.onMessageReactionRemove); */
    await this.client.login(this.token);

    const keys = await redis.keys("*");
    keys.forEach(async k => {
      if (!_.isFinite(_.toNumber(k))) redis.del(k);
      
      const { guildId, channelId, messageId } = JSON.parse(await redis.get(k)) as RedisReactionObject;
      this.reactionWorker.set(k, new RoleReactionWorker(this.client, guildId, channelId, messageId ))
      console.log("Created worker", channelId);
    })

  }

  private async onReady(this: Discord.Client) {
    console.log(`Logged in as ${this.user.tag}!`);
    
  }

  private async onMessage(msg: Discord.Message) {
    //Only serverowner
    if (msg.author.id === msg.guild.ownerID) {
      if (msg.content.startsWith("!")) {
        const messageWithout = msg.content.substr(1, msg.content.length - 1);
        const args = messageWithout.split(" ").map(m => m.toLowerCase())
        const [cmd, ...rest] = args;
        if (cmd === "register") {
          const [ channelInput, messageInput ] = rest;
          if (!channelInput) return msg.reply("Sorry but you need to supply a channel!")
          if (!messageInput) return msg.reply("Sorry but you need to supply a messageId!")
          
          const channel = _.isFinite(_.toNumber(channelInput)) ? msg.guild.channels.resolve(channelInput) as Discord.TextChannel : msg.mentions.channels.first();
          if (!channel) return msg.reply("The channel you supplied is invalid!")
          
          await channel.messages.fetchPinned();
          const message = channel.messages.resolve(messageInput);
          
          if (!message) return msg.reply(`The messageId you supplied is invalid, or does not belong to the channel (did you forgot to pin the message?): <#${channel.id}>!`)

          if (await redis.get(message.id)) return msg.reply("This message has already been registerd. Did you try to add reaction options to id instead?")

          redis.set(message.id, JSON.stringify({
            messageId: message.id,
            channelId: channel.id,
            guildId: msg.guild.id,
            reactions: []
          }))

          return msg.reply("Okay, role reaction message has been registerd. You may now add roles `!addRoleReaction <msg> <emoji> <role>`");
        } else if (cmd === "unregister") {
          const [ messageInput ] = rest;
          if (await redis.get(messageInput)) {
            redis.del(messageInput)
            const worker = this.reactionWorker.get(msg.id);
            worker.Destroy();
            this.reactionWorker.delete(msg.id);
            return msg.reply("Okay role reaction message has been unregisterd.")
          } else {
            return msg.reply("This message has not been registerd.")
          }
        } else if (cmd === "addrolereaction") {
          const [ messageInput, emojiInput, roleInput ] = rest;
          const redisData = await redis.get(messageInput)
          if (redisData) { 
            const data = JSON.parse(redisData) as RedisReactionObject;
            
            // Emojipart - super akward :S
            const encoded = encodeURIComponent(emojiInput);
            const guildEmoji = msg.guild.emojis.cache.find(e => e.toString().toLowerCase() === emojiInput);
            const emoji = encoded.startsWith("%F0%9F") ? encoded : guildEmoji ? guildEmoji.identifier : null;
            if (!emoji) return msg.reply("This is either not an emoji or it is not compatible!");

            await msg.guild.roles.fetch();
            const role =_.isFinite(_.toNumber(roleInput)) ? msg.guild.roles.resolve(roleInput) : msg.mentions.roles.first();
            if (!role) return msg.reply("This role us unknown!");
            
            if (data.reactions.find(r => r.emoji === emoji)) return msg.reply("This reaction has already been registerd to that message! Consider updating or removing it");
            data.reactions.push({
              emoji,
              role: role.id
            })

            const channel = msg.guild.channels.resolve(data.channelId) as Discord.TextChannel
            await channel.messages.fetchPinned();
            const message = channel.messages.resolve(data.messageId);
            await message.react(emoji);
            redis.set(data.messageId, JSON.stringify(data));

            return msg.reply("Okay message reaction has been registerd")
          } else {
            return msg.reply("This message has not been registerd.")
          }
        } else if (cmd === "removerolereaction") {
          const [ messageInput, emojiInput ] = rest;
          const redisData = await redis.get(messageInput)
          if (redisData) { 
            const data = JSON.parse(redisData) as RedisReactionObject;
            
            // Emojipart - super akward :S
            const encoded = encodeURIComponent(emojiInput);
            const guildEmoji = msg.guild.emojis.cache.find(e => e.toString().toLowerCase() === emojiInput);
            const emoji = encoded.startsWith("%F0%9F") ? encoded : guildEmoji ? guildEmoji.identifier : null;
            if (!emoji) return msg.reply("This is either not an emoji or it is not compatible!");

            if (!data.reactions.find(r => r.emoji === emoji)) return msg.reply("This reaction has not been registerd to this message");
            data.reactions = data.reactions.filter(r => r.emoji !== emoji) 

            const channel = msg.guild.channels.resolve(data.channelId) as Discord.TextChannel
            await channel.messages.fetch();
            const message = channel.messages.resolve(data.messageId);

            message.reactions.cache.filter(r => msg.guild.emojis.resolveIdentifier(r.emoji) === emoji).forEach(r => r.remove());
            redis.set(data.messageId, JSON.stringify(data));

            return msg.reply("Okay message reaction has been unregisterd")
          } else {
            return msg.reply("This message has not been registerd.")
          }
        }
      }
    }
  }
}

export default Bot;





/* if (cmd === "channel") {
          const [ channelId ] = rest;
          if (msg.guild.channels.resolve(channelId)) {
            redis.set("channelId", channelId)
            msg.reply("Okay, reaction channel was set!");
          }
        } else if (cmd === "message") {
          const [ msgId ] = rest;
          const channelId = await redis.get("channelId")
          if (!channelId) return msg.reply("You did not set the reaction channel id yet");
  
          const channel = msg.guild.channels.resolve(channelId) as Discord.TextChannel;
          if (channel) {
            if (channel.messages.resolve(msgId)) {
              redis.set("messageId", msgId)
              msg.reply("Okay, reaction message was set!");
            } else {
              return msg.reply("This message id does not exist in the given channel");
            }
          } else {
            return msg.reply("The channel ID i have is invalid");
          }
        }
      } */