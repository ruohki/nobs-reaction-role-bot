import * as Redis from 'ioredis';

const { REDIS } = process.env;
if (!REDIS) {
  console.error("No REDIS present in environment");
  process.exit(-1)
}

export const redis = new Redis(REDIS);