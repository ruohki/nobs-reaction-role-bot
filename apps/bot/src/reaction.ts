import * as Discord from 'discord.js';
import { redis } from './redis';
import { RedisReactionObject } from './bot'

class RoleReaction {
  private channelId: string;
  private messageId: string;
  private guildId: string;
  private client: Discord.Client;

  constructor(client: Discord.Client, guildId: string, channelId: string, messageId: string) {
    this.channelId = channelId;
    this.messageId = messageId;
    this.guildId = guildId;
    this.client = client;

    client.on('messageReactionAdd', this.onMessageReactionAdd.bind(this))
    client.on('messageReactionRemove', this.onMessageReactionRemove.bind(this))

    const guild = client.guilds.resolve(guildId);
    
    const handle = setInterval(() => {
      const channel = guild.channels.resolve(channelId) as Discord.TextChannel;
      if (channel) {
        channel.messages.fetch().then(() => clearInterval(handle))
        console.log("Fetched channel", channelId)
      }
    }, 500)
  }
  public Destroy() {
    this.client.removeListener('messageReactionAdd', this.onMessageReactionAdd.bind(this));
    this.client.removeListener('messageReactionRemove', this.onMessageReactionRemove.bind(this));
  }

  private async onMessageReactionAdd(reaction: Discord.MessageReaction, user: Discord.User | Discord.PartialUser) {
    if (user.bot) return;
    if (reaction.message.id !== this.messageId) return;
    const data = JSON.parse((await redis.get(this.messageId))) as RedisReactionObject
    const info = data.reactions.find(r => r.emoji === reaction.emoji.identifier)
    if (info) {
      const guild = this.client.guilds.resolve(this.guildId);
      const member = guild.members.resolve(user.id)
      member.roles.add(info.role);
      console.log("Granted user", user.username, "role", guild.roles.resolve(info.role).name);
    } else {
      reaction.remove();
    }
  }
  
  private async onMessageReactionRemove(reaction: Discord.MessageReaction, user: Discord.User | Discord.PartialUser) {
    if (user.bot) return;
    if (reaction.message.id !== this.messageId) return;
    const data = JSON.parse((await redis.get(this.messageId))) as RedisReactionObject
    const info = data.reactions.find(r => r.emoji === reaction.emoji.identifier)
    if (info) {
      const guild = this.client.guilds.resolve(this.guildId);
      const member = guild.members.resolve(user.id)
      member.roles.remove(info.role);
      console.log("Revoked user", user.username, "role", guild.roles.resolve(info.role).name);
    }
  }
}

export default RoleReaction;