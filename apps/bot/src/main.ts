import * as Discord from 'discord.js';

import Bot from './bot';

(async () => {
  const { TOKEN } = process.env;
  if (!TOKEN) {
    console.error("No TOKEN present in environment");
    process.exit(-1);
  }

  const bot = new Bot(TOKEN);
  bot.Login();
})();

/* 
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message',async msg => {
  if (msg.author.id === msg.guild.ownerID) {
    if (msg.content.startsWith("!")) {
      const message = msg.content.substr(1, msg.content.length - 1);
      const args = message.split(" ").map(m => m.toLowerCase())
      const [cmd, ...rest] = args;

      if (cmd === "channel") {
        const [ channelId ] = rest;
        if (msg.guild.channels.resolve(channelId)) {
          redis.set("channelId", channelId)
          msg.reply("Okay, reaction channel was set!");
        }
      } else if (cmd === "message") {
        const [ msgId ] = rest;
        const channelId = await redis.get("channelId")
        if (!channelId) return msg.reply("You did not set the reaction channel id yet");

        const channel = msg.guild.channels.resolve(channelId) as Discord.TextChannel;
        if (channel) {
          if (channel.messages.resolve(msgId)) {
            redis.set("messageId", msgId)
            msg.reply("Okay, reaction message was set!");
          } else {
            return msg.reply("This message id does not exist in the given channel");
          }
        } else {
          return msg.reply("The channel ID i have is invalid");
        }
      }
    }
  } else {}
});
 */


//client.login(TOKEN);